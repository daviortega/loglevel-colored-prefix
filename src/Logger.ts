import chalk from "chalk";
import log from "loglevel";
import prefix from "loglevel-plugin-prefix";

const colors = {
  TRACE: chalk.magenta,
  // tslint:disable-next-line: object-literal-sort-keys
  DEBUG: chalk.cyan,
  INFO: chalk.blue,
  WARN: chalk.yellow,
  ERROR: chalk.red
};

type colorType = keyof typeof colors;

type LogLevelDescType = log.LogLevelDesc;

class Logger {
  private readonly log: log.RootLogger;

  constructor(logLevel: log.LogLevelDesc) {
    this.log = log.noConflict();
    this.log.setLevel(logLevel);
    prefix.reg(this.log);
    prefix.apply(this.log, {
      format(level, name, timestamp) {
        const niceLevel = level.toUpperCase() as colorType;
        return `${timestamp} :: ${colors[niceLevel](
          niceLevel.toUpperCase()
        )} :: ${chalk.cyan(name as string)} : `;
      }
    });
    this.log.debug("Logger has been created");
  }

  public getLogger(name: string) {
    return this.log.getLogger(name);
  }
}

export { Logger, LogLevelDescType };
