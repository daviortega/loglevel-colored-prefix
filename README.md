# loglevel-colored-prefix

Simple wrapper around loglevel to add color tags. Written in TypeScript.
It is not sophisticated and it is experimental. But I think it does the trick.

## Usage

```javascript
import * as Logger from "loglevel-colored-prefix";

const logLevel = 'INFO'
const logger = new Logger(logLevel)
const log = logger.getLogger('logThisStuff')

log.info('Hello there!')
// 16:10:09 :: INFO :: logThisStuff : Hello there!
```
