import chai from 'chai'
import { Logger } from '../src/Logger'

const expect = chai.expect

describe('Logger', () => {
	it('should log stuff. It should work and not break. Not testing behavior', () => {
    const logLevel = 'INFO'
    const logger = new Logger(logLevel)
    const log = logger.getLogger('logThisStuff')
    log.info('Hello there!')
	})
})
